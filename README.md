# Version
Node: 12.16.2
NPM: 6.14.4

# Clone and Install

Ejecutar el comando 'clone' de git del depositorio

```
git clone https://gitlab.com/ojsg140789/server.git
```
Con la linea de comando accedes a la carpeta 'server'

Ejecutas el comando para instalar las dependencias:

```
npm install
```

### Run the Application
Para iniciar json-server simplemente ejecutamos el comando:

```
npm start
```

Ahora podemos ver json-server corriendo en :

[`localhost:3000`]
